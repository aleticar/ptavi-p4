#!/usr/bin/python3
""""HOLS ."""
import socket
import sys

try:
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    LINE = sys.argv[3]
    ADDRESS = sys.argv[4]
    EXPIRE = sys.argv[5]

    if LINE == 'register':
        petition1 = "REGISTER SIP : " + str(ADDRESS)
        petition2 = " SIP/2.0\r\n" + 'Expire ' + str(EXPIRE)
        petition = petition1 + petition2

except IndexError:
    sys.exit('Usage: client.py ip puerto register sip_address expires_value')


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", petition)
    my_socket.send(bytes(petition, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
