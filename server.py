#!/usr/bin/python3
""""HOLA ."""
import socketserver
import sys
import json
import time


class SIPRegisterHandler (socketserver.DatagramRequestHandler):
    """"CLASE ."""

    dicc = {}
    Lista = []

    def register2json(self):
        """"MIRO REGISTRO ."""
        with open('registered.json', 'w') as fichjson:
            json.dump(self.dicc, fichjson, indent=3)
            print("Guardo información del cliente en archivo .json")

    def json2register(self):
        """"REGISTRO NUEVO USUARIO ."""
        try:
            with open("registered.json", "r") as jsonfile:
                self.dicc = json.load(jsonfile)  # Carga su Username
                print("Recargo información de Usuario ya registrado")

        except KeyError:

            pass  # Si no lo encuentra pasa el siguiente

    def handle(self):
        """"MANEJADOR ."""
        self.json2register()
        print(self.client_address)
        line = self.rfile.read()  # =bucle
        print("El cliente nos manda ", str(line.decode('utf-8')))
        Peti = line.decode("utf-8")
        NewPeti = Peti.split(" ")  # salto al del decode
        if NewPeti[0] == "REGISTER":
            New = NewPeti[3].split(":")  # para guardar el usuario despues de :
            Username = New[0]
            IP = self.client_address[0]
            PORT = self.client_address[1]
            EXPIRE = NewPeti[5].split("\r\n")
            Expire = EXPIRE[0]
            TIME = time.time() + int(Expire)
            TimExp = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(TIME))
            AcTi = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))

            self.dicc[Username] = [IP, PORT, TimExp, time.timezone]

            print("Guardo :", self.dicc, '\r\n')

            if TimExp <= AcTi:
                del self.dicc[Username]

                print("Su tiempo de expire es 0 por lo que no se guarda")

        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')

        self.register2json()


if __name__ == "__main__":

    puerto = int(sys.argv[1])
    serv = socketserver.UDPServer(('', puerto), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
